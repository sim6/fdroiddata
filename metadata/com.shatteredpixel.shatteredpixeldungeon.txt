Categories:Games
License:GPLv3+
Web Site:http://shatteredpixel.tumblr.com
Source Code:https://github.com/00-Evan/shattered-pixel-dungeon
Issue Tracker:https://github.com/00-Evan/shattered-pixel-dungeon/issues
Bitcoin:1LyLJAzxCfieivap1yK3iCpGoUmzAnjdyK

Auto Name:Shattered Pixel Dungeon
Summary:Rogue-like
Description:
Traditional roguelike game with pixel-art graphics and simple interface. Based
on [[com.watabou.pixeldungeon]].
.

Repo Type:git
Repo:https://github.com/00-Evan/shattered-pixel-dungeon

Build:0.2.2a,14
    commit=127c74696ade0be0c3824d9373f49b8d9b6e6179
    srclibs=PDClasses-shattered@f2bf9c65809f50ffd372f7d5e3df181cf970f892
    prebuild=cp -R $$PDClasses-shattered$$/com/watabou/* src/com/shatteredpixel/ && \
        echo -e 'java.source=1.7\njava.target=1.7\njava.encoding=ISO-8859-1' >> ant.properties
    target=android-19

Build:0.2.2b,15
    commit=e58df1bc9ccb77fd3218775639650f75a5865172
    srclibs=PDClasses-shattered@f2bf9c65809f50ffd372f7d5e3df181cf970f892
    prebuild=cp -R $$PDClasses-shattered$$/com/watabou/* src/com/shatteredpixel/ && \
        echo -e 'java.source=1.7\njava.target=1.7\njava.encoding=ISO-8859-1' >> ant.properties
    target=android-19

Build:0.2.3a,27
    commit=513b1213bcd7ef885c8d21980c3f4ac703fe035f
    srclibs=PDClasses-shattered@0d890613f07b065d6f2f9b225ddf13564fdb128f
    prebuild=cp -R $$PDClasses-shattered$$/com/watabou/* src/com/shatteredpixel/ && \
        echo -e 'java.source=1.7\njava.target=1.7\njava.encoding=ISO-8859-1' >> ant.properties
    target=android-19

Build:0.2.3c,29
    commit=fd375b80e3a51c0811798bf34e58bc07383f3051
    srclibs=PDClasses-shattered@7c21c8107bd85e9c572c68018f1d7e2e2f941f50
    prebuild=cp -R $$PDClasses-shattered$$/com/watabou/* src/com/shatteredpixel/ && \
        echo -e 'java.source=1.7\njava.target=1.7\njava.encoding=ISO-8859-1' >> ant.properties
    target=android-19

Build:0.2.3d,30
    commit=2119f0d1b904db819b48204edb149f32650fad13
    srclibs=PDClasses-shattered@7c21c8107bd85e9c572c68018f1d7e2e2f941f50
    prebuild=cp -R $$PDClasses-shattered$$/com/watabou/* src/com/shatteredpixel/ && \
        echo -e 'java.source=1.7\njava.target=1.7\njava.encoding=ISO-8859-1' >> ant.properties
    target=android-19

Build:0.2.3e,31
    commit=520131fb2d3cae77a741c36cd3016121df76d0c4
    srclibs=PDClasses-shattered@7c21c8107bd85e9c572c68018f1d7e2e2f941f50
    prebuild=cp -R $$PDClasses-shattered$$/com/watabou/* src/com/shatteredpixel/ && \
        echo -e 'java.source=1.7\njava.target=1.7\njava.encoding=ISO-8859-1' >> ant.properties
    target=android-19

Build:0.2.3f,32
    commit=11d550cbe0d4136c78574cec0bda6ddb8af2b6da
    srclibs=PDClasses-shattered@7c21c8107bd85e9c572c68018f1d7e2e2f941f50
    prebuild=cp -R $$PDClasses-shattered$$/com/watabou/* src/com/shatteredpixel/ && \
        echo -e 'java.source=1.7\njava.target=1.7\njava.encoding=ISO-8859-1' >> ant.properties
    target=android-19

Build:0.2.4b,35
    commit=22d072f3e7e
    srclibs=PDClasses-shattered@35fad7951a
    prebuild=cp -R $$PDClasses-shattered$$/com/watabou/* src/com/shatteredpixel/ && \
        echo -e 'java.source=1.7\njava.target=1.7\njava.encoding=ISO-8859-1' >> ant.properties
    target=android-19

Build:0.2.4c,36
    commit=60076db78c203f33b49be0f1f957af9821d9e306
    srclibs=PDClasses-shattered@5c7c4238836ce468fc88f083b85a5d775877d988
    prebuild=cp -R $$PDClasses-shattered$$/com/watabou/* src/com/shatteredpixel/ && \
        echo -e 'java.source=1.7\njava.target=1.7\njava.encoding=ISO-8859-1' >> ant.properties
    target=android-19

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:0.2.4c
Current Version Code:36

