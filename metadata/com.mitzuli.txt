Categories:Office
License:GPLv2+
Web Site:http://www.mitzuli.com
Source Code:https://github.com/artetxem/mitzuli
Issue Tracker:https://github.com/artetxem/mitzuli/issues

Auto Name:Mitzuli
Summary:Offline Translator
Description:
Translator featuring a full offline mode, voice input (ASR), camera input
(OCR), voice output (TTS), and more!
.

Repo Type:git
Repo:https://github.com/artetxem/mitzuli

Build:1.0.0,10000
    disable=crash
    commit=f04cadfae0a02b828e9e3265fcca52923a6655cb
    subdir=app
    gradle=yes
    prebuild=echo 'ndkdir=$$NDK$$' > ../gradle.properties
    scanignore=app/src/main/java/com/mitzuli/core/mt/MtPackage.java
    ndk=r10d

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.0.1
Current Version Code:10001

