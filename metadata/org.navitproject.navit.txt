Categories:Navigation
License:GPLv2
Web Site:http://www.navit-project.org
Source Code:http://sourceforge.net/p/navit/code
Issue Tracker:http://trac.navit-project.org

Name:Navit
Summary:Car navigation system
Description:
Its modular design is capable of using vector maps of various formats
for routing and rendering of the displayed map. It's even possible to
use multiple maps at a time.

The routing engine not only calculates an optimal route to your
destination, but also generates directions and even speaks to you.

Navit currently speaks 27 languages.
You can help translating via the web-based
[http://translations.launchpad.net/navit/trunk/+pots/navit translation page].
.

#http://wiki.navit-project.org/index.php/Android_development
Repo Type:git-svn
Repo:https://svn.code.sf.net/p/navit/code/trunk

Build:5650,5650
    commit=5650
    prebuild=echo '<project name="Navit"><target name="clean"/></project>' > build.xml
    update=no
    build=export PATH=$PATH:$$SDK$$/tools:$$SDK$$/platform-tools:$$NDK$$/toolchains/arm-linux-androideabi-4.8/prebuilt/linux-x86/bin && \
        rm build.xml && \
        mkdir android-build && \
        cd android-build/ && \
        cmake -DCMAKE_TOOLCHAIN_FILE=../navit/Toolchain/arm-eabi.cmake -DDISABLE_QT=true ../navit && \
        make && \
        make android_resources && \
        cd .. && \
        ln -s android-build/navit/android/* .
    novcheck=yes

Build:5720,5720
    commit=5720
    prebuild=echo '<project name="Navit"><target name="clean"/></project>' > build.xml
    update=no
    build=export PATH=$PATH:$$SDK$$/tools:$$SDK$$/platform-tools:$$NDK$$/toolchains/arm-linux-androideabi-4.8/prebuilt/linux-x86/bin && \
        rm build.xml && \
        mkdir android-build && \
        cd android-build/ && \
        cmake -DCMAKE_TOOLCHAIN_FILE=../navit/Toolchain/arm-eabi.cmake -DDISABLE_QT=true ../navit && \
        make && \
        make android_resources && \
        cd .. && \
        ln -s android-build/navit/android/* .
    novcheck=yes

Build:5827,5827
    commit=5827
    prebuild=echo '<project name="Navit"><target name="clean"/></project>' > build.xml
    update=no
    build=export PATH=$PATH:$$SDK$$/tools:$$SDK$$/platform-tools:$$NDK$$/toolchains/arm-linux-androideabi-4.8/prebuilt/linux-x86/bin && \
        rm build.xml && \
        mkdir android-build && \
        cd android-build/ && \
        cmake -DCMAKE_TOOLCHAIN_FILE=../navit/Toolchain/arm-eabi.cmake -DDISABLE_QT=true ../navit && \
        make && \
        make android_resources && \
        cd .. && \
        ln -s android-build/navit/android/* .
    novcheck=yes

Build:5830,5830
    commit=5830
    prebuild=echo '<project name="Navit"><target name="clean"/></project>' > build.xml
    update=no
    build=export PATH=$PATH:$$SDK$$/tools:$$SDK$$/platform-tools:$$NDK$$/toolchains/arm-linux-androideabi-4.8/prebuilt/linux-x86/bin && \
        rm build.xml && \
        mkdir android-build && \
        cd android-build/ && \
        cmake -DCMAKE_TOOLCHAIN_FILE=../navit/Toolchain/arm-eabi.cmake -DDISABLE_QT=true ../navit && \
        make && \
        make android_resources && \
        cd .. && \
        ln -s android-build/navit/android/* .
    novcheck=yes

Build:5889,5889
    commit=5889
    prebuild=echo '<project name="Navit"><target name="clean"/></project>' > build.xml
    update=no
    build=export PATH=$PATH:$$SDK$$/tools:$$SDK$$/platform-tools:$$NDK$$/toolchains/arm-linux-androideabi-4.8/prebuilt/linux-x86/bin && \
        rm build.xml && \
        mkdir android-build && \
        cd android-build/ && \
        cmake -DCMAKE_TOOLCHAIN_FILE=../navit/Toolchain/arm-eabi.cmake -DDISABLE_QT=true ../navit && \
        make && \
        make android_resources && \
        cd .. && \
        ln -s android-build/navit/android/* .
    novcheck=yes

Build:6010,6010
    commit=6010
    srclibs=ACRA@acra-4.3.1
    rm=navit/navit/android/libs/acra-4.3.0b2.jar
    prebuild=echo '<project name="Navit"><target name="clean"/></project>' > build.xml
    update=no
    build=export PATH=$PATH:$$SDK$$/tools:$$SDK$$/platform-tools:$$NDK$$/toolchains/arm-linux-androideabi-4.8/prebuilt/linux-x86/bin && \
        rm build.xml && \
        mkdir android-build && \
        cd android-build/ && \
        cmake -DCMAKE_TOOLCHAIN_FILE=../navit/Toolchain/arm-eabi.cmake -DDISABLE_QT=true ../navit && \
        make && \
        make android_resources && \
        cd .. && \
        rm android-build/navit/android/local.properties && \
        ln -s android-build/navit/android/* . && \
        pushd $$ACRA$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$ACRA$$/target/acra-4.3.1.jar libs/
    novcheck=yes

Build:6012,6012
    commit=6012
    srclibs=ACRA@acra-4.3.1
    rm=navit/navit/android/libs/acra-4.3.0b2.jar
    prebuild=echo '<project name="Navit"><target name="clean"/></project>' > build.xml
    update=no
    build=export PATH=$PATH:$$SDK$$/tools:$$SDK$$/platform-tools:$$NDK$$/toolchains/arm-linux-androideabi-4.8/prebuilt/linux-x86/bin && \
        rm build.xml && \
        mkdir android-build && \
        cd android-build/ && \
        cmake -DCMAKE_TOOLCHAIN_FILE=../navit/Toolchain/arm-eabi.cmake -DDISABLE_QT=true ../navit && \
        make && \
        make android_resources && \
        cd .. && \
        rm android-build/navit/android/local.properties && \
        ln -s android-build/navit/android/* . && \
        pushd $$ACRA$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$ACRA$$/target/acra-4.3.1.jar libs/
    novcheck=yes

Build:6017,6017
    commit=6017
    srclibs=ACRA@acra-4.3.1
    rm=navit/navit/android/libs/acra-4.3.0b2.jar
    prebuild=echo '<project name="Navit"><target name="clean"/></project>' > build.xml
    update=no
    build=export PATH=$PATH:$$SDK$$/tools:$$SDK$$/platform-tools:$$NDK$$/toolchains/arm-linux-androideabi-4.8/prebuilt/linux-x86/bin && \
        rm build.xml && \
        mkdir android-build && \
        cd android-build/ && \
        cmake -DCMAKE_TOOLCHAIN_FILE=../navit/Toolchain/arm-eabi.cmake -DDISABLE_QT=true ../navit && \
        make && \
        make android_resources && \
        cd .. && \
        rm android-build/navit/android/local.properties && \
        ln -s android-build/navit/android/* . && \
        pushd $$ACRA$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$ACRA$$/target/acra-4.3.1.jar libs/
    novcheck=yes

Maintainer Notes:
Found JAR file at navit/navit/android/libs/TTS_library_stub.jar
.

Auto Update Mode:None
Update Check Mode:RepoTrunk
Current Version:6026
Current Version Code:6026

