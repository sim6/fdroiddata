Categories:Multimedia
License:GPLv3
Web Site:https://github.com/souch/SMP/blob/HEAD/README.md
Source Code:https://github.com/souch/SMP
Issue Tracker:https://github.com/souch/SMP/issues
Donate:http://rodolphe.souchaud.free.fr/donate/

Auto Name:SicMu Player
Summary:Play music files
Description:
Lightweight audio player which features a single over-all playlist
with every song on the device. Can be sorted and grouped by folders,
genres, artists, albums and album's track. It works on old slow small
devices (from Android froyo 2.2). No eye candy graphics.
.

Repo Type:git
Repo:https://github.com/souch/SMP

Build:0.2,2
    commit=7e7a933845cf424da1969fa6591e80bdbe75c944
    subdir=app
    gradle=yes

Build:0.3,3
    commit=v0.3
    subdir=app
    gradle=yes

Build:0.3.1,4
    commit=v0.3.1
    subdir=app
    gradle=yes

Build:0.4,5
    commit=v0.4
    subdir=app
    gradle=yes

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:0.4
Current Version Code:5

